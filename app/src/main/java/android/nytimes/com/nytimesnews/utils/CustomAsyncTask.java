package android.nytimes.com.nytimesnews.utils;

import android.app.Activity;
import android.nytimes.com.nytimesnews.R;
import android.nytimes.com.nytimesnews.beans.ResponseBean;
import android.nytimes.com.nytimesnews.dialogs.ProgressDialogFragment;
import android.os.AsyncTask;

import java.io.InterruptedIOException;


public abstract class CustomAsyncTask extends AsyncTask<Object, Object, Object> {

    public abstract void doBackGroundWork() throws Exception;

    public abstract AsyncTask getRetryAsync();

    public Activity activity;
    public boolean isRunning = false;

    protected boolean withDialog = false;

    String loadingMessage;
    String noConnectionMessage;

    Runnable noConnectionRunnable = null;
    Runnable retryRunnable = null;

    protected ProgressDialogFragment dialog;
    public OnFinishListener mOnFinishListener;
    OnNoInternetConnection onNoInternetConnection;

    protected ResponseBean responseBean;

    public CustomAsyncTask(Activity activity, OnFinishListener mOnFinishListener) {
        this.activity = activity;
        this.mOnFinishListener = mOnFinishListener;
        this.responseBean = new ResponseBean();
        loadingMessage = activity.getString(R.string.loading);
        noConnectionMessage = activity.getString(R.string.no_internet_connection_msg);
    }


    public CustomAsyncTask setProgressDialogMsg(String loadingMessage) {
        this.loadingMessage = loadingMessage;
        return this;
    }

    public CustomAsyncTask setWithDialog(boolean withDialog) {
        this.withDialog = withDialog;
        return this;
    }

    public CustomAsyncTask setCheckConnection(boolean checkConnection) {
        this.checkConnection = checkConnection;
        return this;
    }

    public CustomAsyncTask setFinishActivity(boolean finishActivity) {
        this.finishActivity = finishActivity;
        return this;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        isRunning = true;

        try {
            if (withDialog) {
                dialog = GlobalFunctions.getProgressDialog(loadingMessage);
                dialog.show(activity.getFragmentManager(), null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected Object doInBackground(Object... params) {
        try {
            doBackGroundWork();

        } catch (Exception e) {
            e.printStackTrace();
            if (!(e instanceof InterruptedException) && !(e instanceof InterruptedIOException)) {
                if (showRetryOnFail) {
                    if (!activity.isFinishing())
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (getRetryAsync() != null)
                                    ConnectionUtils.retryDialog(activity, getRetryAsync(), checkConnection, finishActivity, withDialog, loadingMessage, retryRunnable);
                            }
                        });
                }
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        isRunning = false;
        try {
            if (withDialog && !activity.isFinishing())
                dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    boolean checkConnection = true;
    boolean showRetryOnNoConnection = false;
    boolean showNoConnectionMsg = true;
    boolean showRetryOnFail = true;
    boolean finishActivity = false;

    //execute async with internet connection check and to finish activity on retry dialog
    public void executeAsync() {
        if (!checkConnection) {
            this.execute();
        } else {
            if (ConnectionUtils.CheckNetwork(activity)) {
                this.execute();
            } else {
                if (onNoInternetConnection != null) {
                    this.onNoInternetConnection.onNoInternet();
                }
                if (showRetryOnNoConnection) {
                    if (getRetryAsync() != null)
                        ConnectionUtils.retryDialog(activity, getRetryAsync(), checkConnection, finishActivity, withDialog, loadingMessage);
                } else {
                    if (showNoConnectionMsg) {
                        GlobalFunctions.showDialog(activity, noConnectionMessage, noConnectionRunnable);
                    }
                }
            }
        }
    }

    public CustomAsyncTask setOnNoInternetConnection(OnNoInternetConnection onNoInternetConnection) {
        this.onNoInternetConnection = onNoInternetConnection;
        return this;
    }

    public OnNoInternetConnection getOnNoInternetConnection() {
        return this.onNoInternetConnection;
    }

    public interface OnFinishListener {
        void onSuccess(Object object);

        void onError(Object error);
    }

    /**
     * in case not connected to any network...(before execute async)
     */
    public interface OnNoInternetConnection {
        void onNoInternet();
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        isRunning = false;
    }

    @Override
    protected void onCancelled(Object o) {
        super.onCancelled(o);
        isRunning = false;
    }

}
