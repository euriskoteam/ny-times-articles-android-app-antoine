package android.nytimes.com.nytimesnews.beans;

import android.nytimes.com.nytimesnews.utils.GlobalVars;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class ArticleBean implements Serializable {
    private String url = "";
    private String adxKeywords = "";
    private String column = "";
    private String section = "";
    private String byline = "";
    private String type = "";
    private String title = "";
    private String abstract_ = "";
    private String publishedDate = "";
    private String source = "";
    private int id = 0;
    private int assetId = 0;
    private int views = 0;
    private MediaBean media;
    private MediaBean thumbnail;

    public ArticleBean() {
        media = new MediaBean();
        thumbnail = new MediaBean();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAdxKeywords() {
        return adxKeywords;
    }

    public void setAdxKeywords(String adxKeywords) {
        this.adxKeywords = adxKeywords;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getByline() {
        return byline;
    }

    public void setByline(String byline) {
        this.byline = byline;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAbstract() {
        return abstract_;
    }

    public void setAbstract(String abstract_) {
        this.abstract_ = abstract_;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAssetId() {
        return assetId;
    }

    public void setAssetId(int assetId) {
        this.assetId = assetId;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public MediaBean getMedia() {
        return media;
    }

    public void setMedia(MediaBean media) {
        this.media = media;
    }

    public MediaBean getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(MediaBean thumbnail) {
        this.thumbnail = thumbnail;
    }

    public static ArticleBean getArticleBeanFromJson(JSONObject articleObject) throws JSONException {
        ArticleBean articleBean = new ArticleBean();
        if (articleObject.has("url") && !articleObject.isNull("url")) {
            articleBean.setUrl(articleObject.getString("url"));
        }
        if (articleObject.has("adx_keywords") && !articleObject.isNull("adx_keywords")) {
            articleBean.setAdxKeywords(articleObject.getString("adx_keywords"));
        }
        if (articleObject.has("column") && !articleObject.isNull("column")) {
            articleBean.setColumn(articleObject.getString("column"));
        }
        if (articleObject.has("byline") && !articleObject.isNull("byline")) {
            articleBean.setByline(articleObject.getString("byline"));
        }
        if (articleObject.has("type") && !articleObject.isNull("type")) {
            articleBean.setType(articleObject.getString("type"));
        }
        if (articleObject.has("title") && !articleObject.isNull("title")) {
            articleBean.setTitle(articleObject.getString("title"));
        }
        if (articleObject.has("abstract") && !articleObject.isNull("abstract")) {
            articleBean.setAbstract(articleObject.getString("abstract"));
        }
        if (articleObject.has("published_date") && !articleObject.isNull("published_date")) {
            articleBean.setPublishedDate(articleObject.getString("published_date"));
        }
        if (articleObject.has("source") && !articleObject.isNull("source")) {
            articleBean.setSource(articleObject.getString("source"));
        }
        if (articleObject.has("id") && !articleObject.isNull("id")) {
            articleBean.setId(articleObject.getInt("id"));
        }
        if (articleObject.has("asset_id") && !articleObject.isNull("asset_id")) {
            articleBean.setAssetId(articleObject.getInt("asset_id"));
        }
        if (articleObject.has("views") && !articleObject.isNull("views")) {
            articleBean.setId(articleObject.getInt("views"));
        }
        System.out.println(">>>>>>mediaaaaa>>>>>0");
        if (articleObject.has("media") && !articleObject.isNull("media")) {
            System.out.println(">>>>>>mediaaaaa>>>>>1");
            JSONArray mediaArray = articleObject.getJSONArray("media");
            if (mediaArray.length() > 0) {
                System.out.println(">>>>>>mediaaaaa>>>>>2");
                JSONObject jsonObject = mediaArray.getJSONObject(0);
                if (jsonObject.has("media-metadata") && !jsonObject.isNull("media-metadata")) {
                    System.out.println(">>>>>>mediaaaaa>>>>>3");
                    JSONArray metaDataArray = jsonObject.getJSONArray("media-metadata");
                    for (int i = 0; i < metaDataArray.length(); i++) {
                        System.out.println(">>>>>>mediaaaaa>>>>>loop");
                        JSONObject mediaObject = metaDataArray.getJSONObject(i);
                        MediaBean mediaBean = MediaBean.setMediaBeanFromJson(mediaObject);
                        if (mediaBean.getFormat().equals(GlobalVars.IMAGE_FORMAT_SQUARE)) {
                            System.out.println(">>>>>>mediaaaaa>>>>>4");
                            articleBean.setMedia(mediaBean);
                        } else if (mediaBean.getFormat().equals(GlobalVars.IMAGE_FORMAT_THUMBNAIL)) {
                            System.out.println(">>>>>>mediaaaaa>>>>>5");
                            articleBean.setThumbnail(mediaBean);
                        }
                    }
                }
            }
        }

        return articleBean;
    }
}
