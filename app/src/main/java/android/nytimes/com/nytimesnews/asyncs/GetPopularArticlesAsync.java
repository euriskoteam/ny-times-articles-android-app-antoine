package android.nytimes.com.nytimesnews.asyncs;

import android.app.Activity;
import android.nytimes.com.nytimesnews.beans.ArticleBean;
import android.nytimes.com.nytimesnews.beans.ErrorBean;
import android.nytimes.com.nytimesnews.utils.CustomAsyncTask;
import android.nytimes.com.nytimesnews.utils.GlobalVars;
import android.nytimes.com.nytimesnews.utils.HttpUtils;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class GetPopularArticlesAsync extends CustomAsyncTask {

    private String section;
    private String period;

    private ErrorBean errorBean;
    private ArrayList<ArticleBean> articles;

    public GetPopularArticlesAsync(Activity activity, String section, String period, OnFinishListener mOnFinishListener) {
        super(activity, mOnFinishListener);

        this.section = section;
        this.period = period;
        errorBean = new ErrorBean(activity);
        articles = new ArrayList<>();
    }

    @Override
    public void doBackGroundWork() throws Exception {
        HashMap<String, String> headers = new HashMap<>();

        String url = GlobalVars.API_POPULAR_ARTICLES + section + "/" + period + ".json?api-key=" + GlobalVars.KEY_API;
        responseBean = HttpUtils.getHttp(url, headers);
        System.out.println(">>>>>>>response>>>>>>>>>>" + responseBean.getResponse());
        System.out.println(">>>>>>>url>>>>>>>>>>" + url);

        if (responseBean != null && responseBean.getCode() > 100 && responseBean.getCode() < 300) {
            getArticles();
        } else {
            try {
                if (responseBean != null) {
                    errorBean = new ErrorBean(activity, responseBean.getResponse());
                } else {
                    errorBean = new ErrorBean(activity);
                }
            } catch (JSONException e) {
                errorBean = new ErrorBean(activity);
            }
        }

    }

    @Override
    public AsyncTask getRetryAsync() {
        return new GetPopularArticlesAsync(activity, section, period, mOnFinishListener);
    }

    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (responseBean != null && responseBean.getCode() > 100 && responseBean.getCode() < 300) {
            mOnFinishListener.onSuccess(articles);
        } else {
            mOnFinishListener.onError(errorBean);

        }
    }

    private void getArticles() throws JSONException {
        JSONObject responseObject = new JSONObject(responseBean.getResponse());
        if (responseObject.has("results") && !responseObject.isNull("results")) {
            JSONArray articlesArray = responseObject.getJSONArray("results");
            for (int i = 0; i < articlesArray.length(); i++) {
                JSONObject articleObject = articlesArray.getJSONObject(i);
                ArticleBean articleBean = ArticleBean.getArticleBeanFromJson(articleObject);
                articles.add(articleBean);
            }
        }
    }
}
