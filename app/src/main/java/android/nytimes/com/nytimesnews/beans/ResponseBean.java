package android.nytimes.com.nytimesnews.beans;


import java.io.Serializable;

public class ResponseBean implements Serializable {
    private String response = "";
    private int code;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
