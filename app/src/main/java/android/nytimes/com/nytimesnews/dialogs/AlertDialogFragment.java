package android.nytimes.com.nytimesnews.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.nytimes.com.nytimesnews.R;
import android.os.Bundle;
import android.widget.ArrayAdapter;

public class AlertDialogFragment extends DialogFragment {

    public AlertDialogFragment() {
    }

    String title = "";
    String message = "";
    boolean cancelable = false;

    String positiveBtnTxt = "";
    DialogInterface.OnClickListener posClickListener;

    String negativeBtnTxt = "";
    DialogInterface.OnClickListener negClickListener;

    OnDismissListener onDismissListener;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setCancelable(boolean cancelable) {
        this.cancelable = cancelable;
    }


    public void setPositiveButton(String btnTxt, DialogInterface.OnClickListener clickListener) {
        positiveBtnTxt = btnTxt;
        posClickListener = clickListener;
    }


    public void setNegativeButton(String btnTxt, DialogInterface.OnClickListener clickListener) {
        negativeBtnTxt = btnTxt;
        negClickListener = clickListener;
    }

    public void setOnDismissListener(OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    ArrayAdapter<String> arrayAdapter;
    DialogInterface.OnClickListener lstClickListener;

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitAllowingStateLoss();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity(), R.style.AlertDialogStyle);
        if (!message.equals(""))
            dialog.setMessage(message);
        if (!title.equals("")) {
            dialog.setTitle(title);
        }
        setCancelable(cancelable);
        dialog.setCancelable(cancelable);
        if (!negativeBtnTxt.equals("")) {
            dialog.setNegativeButton(negativeBtnTxt, negClickListener);
        }
        if (!positiveBtnTxt.equals("")) {
            dialog.setPositiveButton(positiveBtnTxt, posClickListener);
        }
        if (arrayAdapter != null) {
            dialog.setAdapter(arrayAdapter, lstClickListener);
        }

        AlertDialog diag = dialog.create();
        diag.setCancelable(cancelable);
        diag.setCanceledOnTouchOutside(cancelable);

        return diag;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    public interface OnDismissListener {
        void onDismiss();
    }
}

