package android.nytimes.com.nytimesnews.utils;

public class GlobalVars {

    public final static String LOG_TAG = "NYTimesLog";
    /*
     * API Keys
     *
     */

    public final static String KEY_API = "AtGN3MRaZn77ZaRZgeP68xauPRNawWpp";

    /*
     * API Links
     *
     */

    public final static String DOMAIN = "http://api.nytimes.com/";
    public final static String API_POPULAR_ARTICLES = DOMAIN + "svc/mostpopular/v2/mostviewed/";

    /*
     * Image formats
     *
     */

    public final static String IMAGE_FORMAT_SQUARE = "square320";
    public final static String IMAGE_FORMAT_THUMBNAIL = "Standard Thumbnail";

    /*
     * Static api params
     *
     */

    public final static String ARTICLES_SECTION = "all-sections";
    public final static String ARTICLES_PERIOD = "7";

    /*
     *
     * Bundle Tags
     *
     */
    public final static String ARTICLE_BEAN_BUNDLE = "article_bean_bundle";
}
