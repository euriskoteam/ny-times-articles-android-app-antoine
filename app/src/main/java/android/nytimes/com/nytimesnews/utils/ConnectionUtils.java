package android.nytimes.com.nytimesnews.utils;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.nytimes.com.nytimesnews.dialogs.AlertDialogFragment;
import android.os.AsyncTask;
import android.os.Handler;


public class ConnectionUtils {

    public static boolean CheckNetwork(Context context) {
        try {
            ConnectivityManager connec = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                Network[] networks = connec.getAllNetworks();
                for (Network network : networks) {
                    NetworkInfo netInfo = connec.getNetworkInfo(network);
                    if ((netInfo.getType() == ConnectivityManager.TYPE_WIFI || netInfo.getType() == ConnectivityManager.TYPE_MOBILE) && netInfo.isConnected()) {
                        return true;
                    }
                }
            } else {
                NetworkInfo wifi = connec.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                NetworkInfo mobile = connec.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                if ((wifi != null && wifi.isConnected()) || (mobile != null && mobile.isConnected()))
                    return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }

    public static void retryDialog(final Activity activity, final AsyncTask<Object, Object, Object> async,
                                   final boolean checkConnection, final boolean finishActivity, final boolean withDialog, final String msg) {

        AlertDialogFragment diag = new AlertDialogFragment();
        diag.setMessage("Please check your internet connection");
        diag.setCancelable(false);
        diag.setPositiveButton("Retry", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (async != null) {
                    if (async instanceof CustomAsyncTask) {
                        ((CustomAsyncTask) async).setProgressDialogMsg(msg);
                        ((CustomAsyncTask) async).setCheckConnection(checkConnection);
                        ((CustomAsyncTask) async).setFinishActivity(finishActivity);
                        ((CustomAsyncTask) async).setWithDialog(withDialog);
                        ((CustomAsyncTask) async).executeAsync();
                    } else {
                        async.execute();
                    }
                }
            }
        });
        diag.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (finishActivity)
                    activity.finish();
            }
        });
        diag.show(activity.getFragmentManager(), null);
    }

    public static void retryDialog(final Activity activity, final AsyncTask<Object, Object, Object> async,
                                   final boolean checkConnection, final boolean finishActivity, final boolean withDialog, final String msg, final Runnable runnable) {

        AlertDialogFragment diag = new AlertDialogFragment();
        diag.setMessage("Please check your internet connection");
        diag.setCancelable(false);
        diag.setPositiveButton("Retry", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (runnable != null) {
                    new Handler().post(runnable);
                }
                if (async != null) {
                    if (async instanceof CustomAsyncTask) {
                        ((CustomAsyncTask) async).setProgressDialogMsg(msg);
                        ((CustomAsyncTask) async).setCheckConnection(checkConnection);
                        ((CustomAsyncTask) async).setFinishActivity(finishActivity);
                        ((CustomAsyncTask) async).setWithDialog(withDialog);
                        ((CustomAsyncTask) async).executeAsync();
                    } else {
                        async.execute();
                    }
                }
            }
        });
        diag.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (finishActivity)
                    activity.finish();
            }
        });
        diag.show(activity.getFragmentManager(), null);
    }
}
