package android.nytimes.com.nytimesnews.activities;

import android.app.Activity;
import android.nytimes.com.nytimesnews.R;
import android.nytimes.com.nytimesnews.adapters.ArticlesAdapter;
import android.nytimes.com.nytimesnews.asyncs.GetPopularArticlesAsync;
import android.nytimes.com.nytimesnews.beans.ArticleBean;
import android.nytimes.com.nytimesnews.beans.ErrorBean;
import android.nytimes.com.nytimesnews.utils.CustomAsyncTask;
import android.nytimes.com.nytimesnews.utils.GlobalFunctions;
import android.nytimes.com.nytimesnews.utils.GlobalVars;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Activity activity;
    RecyclerView rvArticles;
    SwipeRefreshLayout swipeArticles;

    ArticlesAdapter articlesAdapter;
    GetPopularArticlesAsync getPopularArticlesAsync;
    ArrayList<ArticleBean> articles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupViews();
        getArticles();
    }

    private void setupViews(){
        activity = this;
        rvArticles = findViewById(R.id.rvArticles);
        swipeArticles = findViewById(R.id.swipeArticles);

        swipeArticles.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getArticles();
            }
        });
    }

    private void getArticles(){
        swipeArticles.setRefreshing(true);
        getPopularArticlesAsync = new GetPopularArticlesAsync(activity, GlobalVars.ARTICLES_SECTION, GlobalVars.ARTICLES_PERIOD, new CustomAsyncTask.OnFinishListener() {
            @Override
            public void onSuccess(Object object) {
                swipeArticles.setRefreshing(false);
                articles = (ArrayList<ArticleBean>)object;
                setupArticles();
            }

            @Override
            public void onError(Object error) {
                swipeArticles.setRefreshing(false);
                ErrorBean errorBean = (ErrorBean)error;
                GlobalFunctions.showToast(activity,errorBean.getErrorMessage());
            }
        });
        getPopularArticlesAsync.setOnNoInternetConnection(new CustomAsyncTask.OnNoInternetConnection() {
            @Override
            public void onNoInternet() {
                swipeArticles.setRefreshing(false);
                GlobalFunctions.showToast(activity,getString(R.string.no_internet_connection_msg));
            }
        });
        getPopularArticlesAsync.executeAsync();
    }

    private void setupArticles(){
        articlesAdapter = new ArticlesAdapter(activity,articles);
        rvArticles.setAdapter(articlesAdapter);
    }
}
