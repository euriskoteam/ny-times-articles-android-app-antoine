package android.nytimes.com.nytimesnews.beans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class MediaBean implements Serializable {
    private String url = "";
    private String format = "";
    private int width = 0;
    private int height = 0;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public static MediaBean setMediaBeanFromJson(JSONObject mediaObject) throws JSONException {
        MediaBean mediaBean = new MediaBean();
        if (mediaObject.has("url") && !mediaObject.isNull("url")) {
            mediaBean.setUrl(mediaObject.getString("url"));
        }
        if (mediaObject.has("format") && !mediaObject.isNull("format")) {
            mediaBean.setFormat(mediaObject.getString("format"));
        }
        if (mediaObject.has("width") && !mediaObject.isNull("width")) {
            mediaBean.setWidth(mediaObject.getInt("width"));
        }
        if (mediaObject.has("height") && !mediaObject.isNull("height")) {
            mediaBean.setHeight(mediaObject.getInt("height"));
        }

        return mediaBean;
    }
}
