package android.nytimes.com.nytimesnews.activities;

import android.app.Activity;
import android.nytimes.com.nytimesnews.R;
import android.nytimes.com.nytimesnews.beans.ArticleBean;
import android.nytimes.com.nytimesnews.utils.GlobalVars;
import android.nytimes.com.nytimesnews.utils.ImageLoadingUtils;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

public class ArticleDetailsActivity extends AppCompatActivity {

    Activity activity;
    ImageLoader imgLoader;

    ImageView ivPhoto;
    TextView tvTitle, tvDescription, tvByline, tvDate;

    ArticleBean articleBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_details);

        articleBean = (ArticleBean) getIntent().getSerializableExtra(GlobalVars.ARTICLE_BEAN_BUNDLE);
        if(articleBean == null){
            articleBean = new ArticleBean();
        }

        setupViews();
    }

    private void setupViews(){
        activity = this;
        imgLoader = ImageLoadingUtils.getImageLoader(activity);

        ivPhoto = findViewById(R.id.ivPhoto);
        tvTitle = findViewById(R.id.tvTitle);
        tvDescription = findViewById(R.id.tvDescription);
        tvByline = findViewById(R.id.tvByline);
        tvDate = findViewById(R.id.tvDate);

        imgLoader.displayImage(articleBean.getMedia().getUrl(),ivPhoto);
        tvTitle.setText(articleBean.getTitle());
        tvDescription.setText(articleBean.getAbstract());
        tvByline.setText(articleBean.getByline());
        tvDate.setText(articleBean.getPublishedDate());
    }
}
