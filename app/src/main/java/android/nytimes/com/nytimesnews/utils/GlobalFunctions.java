package android.nytimes.com.nytimesnews.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.nytimes.com.nytimesnews.BuildConfig;
import android.nytimes.com.nytimesnews.dialogs.AlertDialogFragment;
import android.nytimes.com.nytimesnews.dialogs.ProgressDialogFragment;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

public class GlobalFunctions {


    public static void showDialog(final Activity activity, String message, final Runnable runnable) {
        try {
            AlertDialogFragment diag = new AlertDialogFragment();
            diag.setMessage(message);
            diag.setCancelable(false);
            diag.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (runnable != null) {
                        new Handler().post(runnable);
                    }
                    dialog.dismiss();
                }
            });
            diag.setOnDismissListener(new AlertDialogFragment.OnDismissListener() {
                @Override
                public void onDismiss() {
                    if (runnable != null) {
                        new Handler().post(runnable);
                    }
                }
            });
            if (!activity.isFinishing())
                diag.show(activity.getFragmentManager(), null);
        } catch (Exception e) {
            printException(e);
        }

    }

    public static ProgressDialogFragment getProgressDialog(String msg) {
        ProgressDialogFragment diag = new ProgressDialogFragment();
        diag.setMessage(msg);
        return diag;
    }

    public static void showToast(Activity activity, String Message) {
        Toast.makeText(activity, Message, Toast.LENGTH_SHORT).show();
    }

    public static void printException(Exception e) {
        if (BuildConfig.DEBUG) {
            if (e != null) {
                Log.d(GlobalVars.LOG_TAG, Log.getStackTraceString(e));
            }
        }
    }
}
