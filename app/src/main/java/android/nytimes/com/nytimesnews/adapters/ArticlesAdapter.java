package android.nytimes.com.nytimesnews.adapters;

import android.app.Activity;
import android.content.Intent;
import android.nytimes.com.nytimesnews.R;
import android.nytimes.com.nytimesnews.activities.ArticleDetailsActivity;
import android.nytimes.com.nytimesnews.beans.ArticleBean;
import android.nytimes.com.nytimesnews.customViews.CircleImageView;
import android.nytimes.com.nytimesnews.utils.GlobalVars;
import android.nytimes.com.nytimesnews.utils.ImageLoadingUtils;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

public class ArticlesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Activity activity;
    ArrayList<ArticleBean> articles;

    private ImageLoader imgLoader;
    private DisplayImageOptions options;
    private LayoutInflater inflater;

    public ArticlesAdapter(Activity activity, ArrayList<ArticleBean> articles) {

        this.activity = activity;
        this.articles = articles;

        inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        imgLoader = ImageLoadingUtils.getImageLoader(activity);
        options = ImageLoadingUtils.getImageOptions(R.drawable.transparent_bg);
    }

    private static class ArticleViewHolder extends RecyclerView.ViewHolder {
        CircleImageView ivPhoto;
        TextView tvTitle,tvByline,tvDate;
        RelativeLayout rlArticle;

        ArticleViewHolder(View convertView) {
            super(convertView);
            ivPhoto = convertView.findViewById(R.id.ivPhoto);
            tvTitle = convertView.findViewById(R.id.tvTitle);
            tvByline = convertView.findViewById(R.id.tvByline);
            tvDate = convertView.findViewById(R.id.tvDate);
            rlArticle = convertView.findViewById(R.id.rlArticle);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.item_article, parent, false);
        return new ArticleViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        if (viewHolder instanceof ArticleViewHolder) {
            ArticleViewHolder holder = (ArticleViewHolder) viewHolder;
            ArticleBean articleBean = articles.get(position);

            imgLoader.displayImage(articleBean.getThumbnail().getUrl(), holder.ivPhoto,options);
            holder.tvTitle.setText(articleBean.getTitle());
            holder.tvByline.setText(articleBean.getByline());
            holder.tvDate.setText(articleBean.getPublishedDate());

            holder.rlArticle.setTag(position);
            holder.rlArticle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int)v.getTag();
                    ArticleBean article = articles.get(pos);
                    Intent intent = new Intent(activity,ArticleDetailsActivity.class);
                    intent.putExtra(GlobalVars.ARTICLE_BEAN_BUNDLE,article);
                    activity.startActivity(intent);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return articles == null ? 0 : articles.size();
    }

}
