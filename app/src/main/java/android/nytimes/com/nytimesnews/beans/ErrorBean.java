package android.nytimes.com.nytimesnews.beans;

import android.app.Activity;
import android.nytimes.com.nytimesnews.R;

import org.json.JSONException;
import org.json.JSONObject;

public class ErrorBean {

    private String errorMessage = "";
    private String errorCode = "";

    public ErrorBean(Activity activity) {
        setErrorMessage(activity.getString(R.string.something_went_wrong));
    }

    public ErrorBean(Activity activity, String response) throws JSONException {
        JSONObject object = new JSONObject(response);

        if (object.has("fault") && !object.isNull("fault")) {
            JSONObject fault = object.getJSONObject("fault");
            if (fault.has("faultstring") && !fault.isNull("faultstring")) {
                setErrorMessage(fault.getString("faultstring"));
            }
            if (fault.has("detail") && !fault.isNull("detail")) {
                JSONObject detail = object.getJSONObject("detail");
                if (detail.has("errorcode") && !detail.isNull("errorcode")) {
                    setErrorCode(detail.getString("errorcode"));
                }
            }
        }
        if (getErrorMessage().isEmpty()) {
            setErrorMessage(activity.getString(R.string.something_went_wrong));
        }
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
