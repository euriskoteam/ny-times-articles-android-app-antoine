# NY Times Popular Articles

NY Times popular articles is a news app that fetches popular articles found on new york times and display them with a detailed view

## Setup



```bash
git clone https://antoine_bassil@bitbucket.org/euriskoteam/ny-times-articles-android-app-antoine.git
```
- Sync Gradle
- Rebuild Project


## IDE Requirements
Android Studio

## Installation
Run the app after connecting to an Android device or an emulator.


## Developer
Antoine Bassil